import React from 'react';
import { PieChart as PieRechart, Pie } from 'recharts';

/**
 * For the documentation and examples go to http://recharts.org
 */
class PieChart extends PieRechart {
    render() {
        return super.render();
    }
}

export { PieChart as default, Pie };

