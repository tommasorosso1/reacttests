import { BarChart as BarRechart, Bar, Legend, Tooltip, CartesianGrid, XAxis, YAxis } from 'recharts';

class BarChart extends BarRechart {
    /**
     * For the documentation and examples go to http://recharts.org
     */
    render() {
        return super.render();
    }
}

export { BarChart as default, XAxis, YAxis, CartesianGrid, Tooltip, Legend, Bar };
